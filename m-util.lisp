;;;; m-util.lisp

(in-package #:m-util)

(defmacro flet-def-and-call ((args function call-with &key (dynamic-extent t))
                             &body body)
  "Generate a single local function with argument list ARGS using flet and apply
FUNCTION with this function as its first argument and CALL-WITH as its remaining
arguments.

Used for defining with-resource macros."
  (let ((f (gensym "CONT")))
    `(flet ((,f ,args ,@body))
       ,@(when dynamic-extent
           `((declare (dynamic-extent #',f))))
       (,function #',f ,@call-with))))

(defmacro without-keys ((keylist &rest keys) &body body)
  "Bind KEYLIST to a copy of KEYLIST and remove KEYS from it with remf."
  `(let ((,keylist (copy-list ,keylist)))
     ,@(mapcar (lambda (key) `(remf ,keylist ,key)) keys)
     ,@body))

(defun genpred (equality-function &rest initial-arguments)
  "A general predicate generation function using currying"
  (lambda (&rest remaining-arguments)
    (apply equality-function (append initial-arguments remaining-arguments))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun macro-genpred (equality-function &rest initial-arguments)
    (if (eql equality-function 'gcase-exempt)
        `(,@initial-arguments)
        `(lambda (&rest remaining-arguments)
           ,(let ((args (if initial-arguments
                            `(append (list ,@initial-arguments)
                                     remaining-arguments)
                            'remaining-arguments)))
              `(apply #',equality-function ,args)))))

  (defun macro-generate-case-predicate (element-variable tree)
    (cond ((atom tree)
           ;; When the tree is an atom we are checking if a single variable is
           ;; truthy or falsey. When the car is a lambda we want to apply the
           ;; literal function. Likewise we want to do the same if the function
           ;; is a literal function (eg #'fn or (function fn)).
           tree)
          ((member (car tree) '(or and not))
           (append (list (car tree))
                   (mapcar (lambda (el)
                             (let ((subel (macro-generate-case-predicate
                                           element-variable
                                           el)))
                               (cond ((atom subel) subel)
                                     ((member (car subel) '(lambda function))
                                      `(funcall ,subel ,element-variable))
                                     (t subel))))
                           (cdr tree))))
          (t `(funcall ,(apply #'macro-genpred tree) ,element-variable)))))

(defmacro gcase (thing &body cases)
  "A generalized version case, which is more similar to cond. Take a single
'keyform', and expand the cases into conditional statements using macro-genpred.

Every function call in the predicate portion of a gcase case is curried and then
funcalled with THING as its final argument. To elide this currying a function
call can be prefixed with gcase-exempt, eg (gcase-exempt boundp '*var*) will be
expanded into (boundp '*var*). 

Example:

(gcase (some-function a b c)
  ((or (string= \"hi\")
       (string= \"hello\"))
   (print 'hello))
  ((or (eql 'hi)
       (and (eql 'hello)
            (gcase-exempt boundp '*some-variable*)))
   (print 'another-case))
  (t (print 'bye)))
"
  (let ((e (gensym)))
    `(let ((,e ,thing))
       (cond ,@(loop for (pred case) in cases
                     collect `(,(macro-generate-case-predicate e pred)
                               ,case))))))

(defun invoke-with-normalized-stream (continuation stream &rest keys)
  (cond ((streamp stream)
         (funcall continuation stream))
        ((pathnamep stream)
         (let ((s (apply 'open stream keys)))
           (unwind-protect (funcall continuation s)
             (close s))))
        ((stringp stream)
         (with-input-from-string (s stream)
           (funcall continuation s)))
        (t
         (error "Dont know how to normalize ~S" stream))))

(defmacro with-normalized-stream ((var stream &rest args) &body body)
  (typecase stream
    (pathname
     `(with-open-file (,var ,stream ,@args)
        ,@body))
    (string
     `(with-input-from-string (,var ,stream)
        ,@body))
    (otherwise
     `(flet-def-and-call ((,var)
                          invoke-with-normalized-stream
                          (,stream ,@args))
        ,@body))))
