
(in-package :m-util)

;;; Process Creation

(define-condition invalid-process-error (error)
  ((process :reader invalid-process-error-process :initarg :process)))

(defvar *saved-processes* ()
  "An alist of program invocation ids and the associated process")

(defun terminate-process (process)
  (let ((el (rassoc process *saved-processes*)))
    (when el
      (setf *saved-processes* (remove el *saved-processes*)))
    (when (typep process 'uiop/launch-program::process-info)
      #-sbcl
      (uiop:terminate-process process)
      #+sbcl
      ;; uiop is broken (at least on sbcl) and wont terminate the child
      ;; processes, so we use killpg to kill the entire process group.
      (let ((pid (uiop:process-info-pid process)))
        (ignore-errors (sb-posix:killpg pid sb-posix:sigterm))))))

(defun terminate-all-processes ()
  (map 'nil
       (lambda (el)
         (let ((process (cdr el)))
           (when (typep process 'uiop/launch-program::process-info)
             #-sbcl
             (uiop:terminate-process process)
             #+sbcl
             ;; uiop is broken (at least on sbcl) and wont terminate the child
             ;; processes, so we use killpg to kill the entire process group.
             (let ((pid (uiop:process-info-pid process)))
               (ignore-errors (sb-posix:killpg pid sb-posix:sigterm))))))
       *saved-processes*)
  (setf *saved-processes* nil))

(defun save-process (id process)
  (push (cons id process) *saved-processes*))

(defun find-process (id)
  (assoc id *saved-processes*
         :test (lambda (e1 e2)
                 (cond ((and (stringp e1) (stringp e2))
                        (string= e1 e2))
                       ((and (symbolp e1) (symbolp e2))
                        (eql e1 e2))
                       ((and (listp e1)
                             (every #'stringp e1)
                             (listp e2)
                             (every #'stringp e2))
                        (not (member nil (mapcar #'string= e1 e2))))))))

(defun invoke-with-process (continuation program &rest launch-program-keys
                            &key reuse (terminate t) (streamio t)
                            &allow-other-keys)
  "Invoke CONTINUATION with a process-info object. Return the result of
CONTINUATION.

When REUSE is true, attempt to find a pre-existing process-info object to use
instead of generating a new one. If a process-info object cannot be found
generate a new process info object by applying uiop:launch-program to an
argument list containing the relevant keys from LAUNCH-PROGRAM-KEYS. If REUSE is
the literal T, then PROGRAM is used as the id to store the process
under, otherwise REUSE is used as the id.

When TERMINATE is true, the process-info object is terminated before returning
from INVOKE-WITH-PROCESS.

When STREAMIO is true, the input, output and error-output for the newly created
process are streams. This will have no effect when reusing a process."
  (without-keys (launch-program-keys :terminate :streamio :reuse)
    (let* ((stream-ioe
             (when streamio
               (list :input :stream :output :stream :error-output :stream)))
           (process
             (flet ((genproc ()
                      (apply #'uiop:launch-program
                             program
                             (append stream-ioe launch-program-keys))))
               (if reuse
                   (let* ((%p (find-process (if (eq reuse t) program reuse)))
                          (p (or (cdr %p) (genproc))))
                     (unless %p
                       (save-process (if (eq reuse t) program reuse) p))
                     p)
                   (genproc)))))
      (unwind-protect (funcall continuation process)
        (when terminate
          (terminate-process process))))))

(defmacro with-process ((var program &rest keys
                         &key (terminate t) (streamio t) reuse &allow-other-keys)
                        &body body)
  "Evaluate BODY with VAR bound to the process-info object for PROGRAM"
  (declare (ignore terminate streamio reuse))
  `(flet-def-and-call ((,var) invoke-with-process (,program ,@keys))
     ,@body))

(defun invoke-with-process-io (continuation process &optional with-error-output)
  "Invoke CONTINUATION with the input and output streams of PROCESS. If
WITH-ERROR-OUTPUT is true then pass the error output stream as well."
  (if (and process
           (typep process 'uiop/launch-program::process-info)
           (uiop:process-alive-p process))
      (apply continuation
             (append (list (uiop:process-info-input process)
                           (uiop:process-info-output process))
                     (when with-error-output
                       (list (uiop:process-info-error-output process)))))
      (error 'invalid-process-error :process process)))

(defmacro with-process-io ((input output process) &body body)
  "Bind INPUT and OUTPUT to the input and output streams of PROCESS."
  `(flet-def-and-call ((,input ,output) invoke-with-process-io (,process))
     ,@body))

(defmacro with-process-ioe ((input output error process) &body body)
  "Bind INPUT and OUTPUT and ERROR to the input, output, and error streams of
PROCESS."
  `(flet-def-and-call ((,input ,output ,error) invoke-with-process-io (,process t))
     ,@body))

(defun sh (command &key (reuse-id 'sh) (terminating-char #\EOT))
  "Run COMMAND under posix SH and return the output and the exit code. Output is
read until TERMINATING-CHAR is encountered. REUSE-ID is the id for the reusable
shell to use."
  (flet ((collect-lines (stream)
           (let (lines)
             (do ((line (read-line stream) (read-line stream)))
                 ((char= (char line 0) terminating-char)
                  (values (apply #'concatenate 'string (reverse lines))
                          (parse-integer (subseq line 1))))
               (if lines
                   (setf lines (cons line (cons '(#\NEWLINE) lines)))
                   (setf lines (cons line lines)))))))
    (with-process (proc '("sh") :terminate nil :reuse reuse-id)
      (with-process-io (proc-i proc-o proc)
        (format proc-i "(~A); echo \"~C$?\"~%" command terminating-char)
        (finish-output proc-i)
        (collect-lines proc-o)))))

(defun collect-shell-command (command &optional (terminating-char #\EOT))
  "Run COMMAND under posix SH and return the output and the exit code. Output is
read until TERMINATING-CHAR is encountered."
  (sh command :reuse-id 'collect-shell-command :terminating-char terminating-char))
