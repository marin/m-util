;;;; package.lisp

(defpackage #:m-util
  (:use #:cl)
  (:export #:gcase
           #:flet-def-and-call
           #:genpred
           #:with-process
           #:invoke-with-process
           #:with-process-io
           #:invoke-with-process-io
           #:invoke-with-process-ioe
           #:collect-shell-command
           #:sh))
