;;;; m-util.asd

(asdf:defsystem #:m-util
  :description "A collection of utilities"
  :author "Marin <marind at posteo dot no>"
  :license "GPLv3"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "m-util")
               (:file "processes")
               (:file "ansi-escape-codes")))
